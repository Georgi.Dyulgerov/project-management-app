# Spring Boot Project Management API #
<br>

## Summary

The Project Management application manages the operations of an IT company. It brings
visibility to an organization of multiple teams, working on multiple projects. 

This project uses Spring JPA, Hibernate, Spring Web, Spring Boot, Spring Security and Docker.

## How to build

1. Clone the repository;
2. Unzip and cd to project root folder (```~/georgi-dyulgerov/Midterm 2/project-management-app```);
3. Make sure you have docker and docker-composer installed;
4. Run command 


```docker-compose up```

or


```docker-compose up -d```


to run it as daemon.

By default the docker compose file is set up without a mounted volume for data persistence of the Oracle DB container. If you wish to change this behaviour add a line to ```docker-compose.yml``` with ```/opt/oracle/oradata``` as shown bellow before building the docker containers with ```docker-compose up```:

	...
	volumes:
	  - ./src/main/resources:/opt/oracle/scripts/setup
	  - ./{PATH}/{TO}/{LOCAL}/{FOLDER}:/opt/oracle/oradata
	  pm-spring-boot:
	...

The first time you build the containers, the Oracle DB will be automatically populated with data from the ```database.sql``` file.

A default user with an **admin** role for the app is created with username: **admin** and password: **adminpass**.

## Used docker images

 - Oracle 19.3.0 EE DB - 
**omgthehorror/oracle-db:19.3.0-ee**


 - Spring Boot Project Management API -
**omgthehorror/pm-spring-boot:pm-spring-boot**


## Endpoints

### Login

  - **Post** request to **'/users/login'** endpoint with valid credentials in the body (JSON) will return a JSON Web Token. This bearer token is then used inside the Authorization header for every other API request. **Tokens are valid for one hour**.

  - **Example Request**


		{
			"username": "test",
			"password": "test"
		}


  - **Example Response**


		{
		    "token": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwic3ViIjoiYWRtaW4iLCJhZG1pbiI6MSwiZXhwIjoxNTg5MjkwNzkyfQ.goBUylFbHxI5Yfs2AYInIG62BhUvgCe7BVidh4Ta9nk"
		}


----------

### Users

#### Create User
 - **Post** request to **'/users'** endpoint with valid token in Authorization header and valid body (JSON) will create a new user.

	***The accessing user must have admin role to be able to create new users.***

 - **Example Request**


		{
		    "username": "test",
		    "password": "test",
		    "firstName": "Test",
		    "lastName": "Test",
		    "isAdmin": 0
		}


 - **Example Response**

	Returns the newly created user:
 

		{
		    "id": 2,
		    "username": "test",
		    "firstName": "Test",
		    "lastName": "Test",
		    "isAdmin": false,
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1
		}

#### Edit User
 - **Put** request to **'/users/{userId}'** endpoint with valid token in Authorization header, valid userId parameter and valid body (JSON) will edit a user.

	***The accessing user must have admin role to be able to edit users.***

 - **Example Request**


		{
		    "username": "test1",
		    "password": "test1",
		    "firstName": "TestEdit",
		    "lastName": "TestEdit",
		    "isAdmin": 1
		}


 - **Example Response**

	Returns the newly edited user:


		{
		    "id": 2,
		    "username": "test1",
		    "firstName": "TestEdit",
		    "lastName": "TestEdit",
		    "isAdmin": true,
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1
		}


#### Get User By Id
 - **Get** request to **'/users/{userId}'** endpoint with valid token in Authorization header and valid userId parameter will return a user by its id.

	***The accessing user must have admin role to be able to get user by id.***

 - **Example Response**

	Returns the user found by id:
 

		{
		    "id": 2,
		    "username": "test1",
		    "firstName": "TestEdit",
		    "lastName": "TestEdit",
		    "isAdmin": true,
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1
		}


#### Get All Users

 - **Get** request to **'/users'** endpoint with valid token in Authorization header will return a list with all users.

	***The accessing user must have admin role to be able to get all users.***

 - **Example Response**

	Returns a list of users:

		[
		    {
		        "id": 2,
		        "username": "test1",
		        "firstName": "TestEdit",
		        "lastName": "TestEdit",
		        "isAdmin": true,
		        "dateCreated": "2020-05-12",
		        "creatorId": 1,
		        "dateLastChanged": "2020-05-12",
		        "lastChangeUserId": 1
		    },
		    {
		        "id": 1,
		        "username": "admin",
		        "firstName": "Admin",
		        "lastName": "Admin",
		        "isAdmin": true,
		        "dateCreated": "2003-05-03",
		        "creatorId": 1,
		        "dateLastChanged": "2003-05-03",
		        "lastChangeUserId": 1
		    }
		]


#### Delete User

 - **Delete** request to **'/users/{userId}'** endpoint with valid token in Authorization header and valid userId parameter will delete a user.

	***The accessing user must have admin role to be able to delete users.***

----------

### Teams

#### Create Team

 - **Post** request to **'/teams'** endpoint with valid token in Authorization header and valid body (JSON) will create a new team.

	***The accessing user must have admin role to be able to create new teams.***

 - **Example Request**


		{
		   "title": "test"
		}


 - **Example Response**

	Returns the newly created team:
 

		{
		    "id": 1,
		    "title": "test",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedUserIds": []
		}


#### Edit Team

 - **Put** request to **'/teams/{teamId}'** endpoint with valid token in Authorization header, valid teamId parameter and valid body (JSON) will create a new team.

	***The accessing user must have admin role to be able to edit teams.***

 - **Example Request**


		{
		   "title": "test1"
		}


 - **Example Response**

	Returns the newly edited team:
 

		{
		    "id": 1,
		    "title": "test1",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedUserIds": []
		}


#### Get Team By Id

 - **Get** request to **'/teams/{teamId}'** endpoint with valid token in Authorization header and valid teamId parameter will return a team by its id.

	***The accessing user must have admin role to be able to get team by id.***

 - **Example Response**

	Returns the team found by id:
 

		{
		    "id": 1,
		    "title": "test",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedUserIds": []
		}


#### Get All Teams

 - **Get** request to **'/teams'** endpoint with valid token in Authorization header will return a list with all teams.

	***The accessing user must have admin role to be able to get all teams.***

 - **Example Response**

	Returns a list of teams:

		[
		    {
		        "id": 2,
		        "title": "test2",
		        "dateCreated": "2020-05-12",
		        "creatorId": 1,
		        "dateLastChanged": "2020-05-12",
		        "lastChangeUserId": 1,
		        "assignedUserIds": []
		    },
		    {
		        "id": 1,
		        "title": "test",
		        "dateCreated": "2020-05-12",
		        "creatorId": 1,
		        "dateLastChanged": "2020-05-12",
		        "lastChangeUserId": 1,
		        "assignedUserIds": []
		    }
		]


#### Delete Team

 - **Delete** request to **'/teams/{teamId}'** endpoint with valid token in Authorization header and valid teamId parameter will delete a team.

	***The accessing user must have admin role to be able to delete teams.***

#### Assign User to Team

 - **Put** request to **'/teams/{teamId}/users/{userId}'** endpoint with valid token in Authorization header, valid teamId parameter and valid userId parameter will assign user to a team.

	***The accessing user must have admin role to be able to assign users to teams.***

 - **Example Response**

	Returns the team with the newly assigned user:
 

		{
		    "id": 1,
		    "title": "test",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedUserIds": [
		        1
		    ]
		}


#### Remove User From Team

 - **Delete** request to **'/teams/{teamId}/users/{userId}'** endpoint with valid token in Authorization header, valid teamId parameter and valid userId parameter will remove user from a team.

	***The accessing user must have admin role to be able to remove users from teams.***

 - **Example Response**

	Returns the team with the removed user:


		{
		    "id": 1,
		    "title": "test",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedUserIds": []
		}


----------

### Projects

#### Create Project

 - **Post** request to **'/projects'** endpoint with valid token in Authorization header and valid body (JSON) will create a new project.

 - **Example Request**


		{
		   "title": "test",
		   "description": "test"
		}


 - **Example Response**

	Returns the newly created project:
 

		{
		    "id": 1,
		    "title": "test",
		    "description": "test",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedTeamIds": []
		}


#### Edit Project

 - **Put** request to **'/projects/{projectId}'** endpoint with valid token in Authorization header, valid projectId parameter and valid body (JSON) will create a new project.

	***The accessing user must be the project's creator to be able to edit the project.***

 - **Example Request**
 

		{
		   "title": "testEdit",
		   "description": "testEdit"
		}


 - **Example Response**

	Returns the newly edited project:
 

		{
		    "id": 1,
		    "title": "testEdit",
		    "description": "testEdit",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedTeamIds": []
		}


#### Get Project By Id

 - **Get** request to **'/projects/{projectId}'** endpoint with valid token in Authorization header and valid projectId parameter will return a project by its id.

	***To see project, the accessing user must be either the project's creator, or/and part of a team assigned to project.***

 - **Example Response**

	Returns the project found by id:
 

		{
		    "id": 1,
		    "title": "testEdit",
		    "description": "testEdit",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedTeamIds": []
		}


#### Get All Projects

 - **Get** request to **'/projects'** endpoint with valid token in Authorization header will return a list with projects.

	***To see project, the accessing user must be either the project's creator, or/and part of a team assigned to project.***

 - **Example Response**

	Returns a list of projects:
 

		[
		    {
		        "id": 2,
		        "title": "test2",
		        "description": "test",
		        "dateCreated": "2020-05-12",
		        "creatorId": 1,
		        "dateLastChanged": "2020-05-12",
		        "lastChangeUserId": 1,
		        "assignedTeamIds": []
		    },
		    {
		        "id": 1,
		        "title": "testEdit",
		        "description": "testEdit",
		        "dateCreated": "2020-05-12",
		        "creatorId": 1,
		        "dateLastChanged": "2020-05-12",
		        "lastChangeUserId": 1,
		        "assignedTeamIds": []
		    }
		]


#### Delete Project

 - **Delete** request to **'/projects/{projectId}'** endpoint with valid token in Authorization header and valid projectId parameter will delete a project.

	***The accessing user must be the project's creator to be able to delete project.***

#### Assign Team to Project

 - **Put** request to **'/projects/{projectId}/teams/{teamId}'** endpoint with valid token in Authorization header, valid projectId parameter and valid teamId parameter will assign team to a project.

	***The accessing user must be the project's creator to be able to assign teams to project.***

 - **Example Response**

	Returns the project with the newly assigned team:
 

		{
		    "id": 1,
		    "title": "testEdit",
		    "description": "testEdit",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedTeamIds": [
		        1
		    ]
		}


#### Remove Team From Project

 - **Delete** request to **'/projects/{projectId}/teams/{teamId}'** endpoint with valid token in Authorization header, valid projectId parameter and valid teamId parameter will remove an assigned team from a project.


	***The accessing user must be the project's creator to be able to remove assigned teams from project.***

 - **Example Response**

	Returns the project with the removed team:
 

		{
		    "id": 1,
		    "title": "testEdit",
		    "description": "testEdit",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1,
		    "assignedTeamIds": []
		}


----------

### Tasks

#### Create Task

 - **Post** request to **'/projects/{parentProjectId}/tasks'** endpoint with valid token in Authorization header, valid parentProjectId parameter and valid body (JSON) will create a new task.

	***The accessing user must be project's creator to be able to create tasks in project.***

 - **Example Request**


		{
		   "title": "test",
		   "description": "test"
		}


 - **Example Response**

	Returns the newly created task:
 

		{
		    "id": 1,
		    "parentProjectId": 1,
		    "assigneeId": null,
		    "title": "test",
		    "description": "test",
		    "status": "PENDING",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1
		}


#### Edit Task

 - **Put** request to **'/projects/{parentProjectId}/tasks/{taskId}'** endpoint with valid token in Authorization header, valid parentProjectId parameter, valid taskId parameter and valid body (JSON) will edit task.

	***The accessing user must be project's creator to be able to edit tasks in project.***

 - **Example Request**
 

		{
		   "assigneeId": "1",
		   "title": "testEdit",
		   "description": "testEdit",
		   "status": 1
		}


 - **Example Response**

	Returns the newly edited task:
 

		{
		    "id": 1,
		    "parentProjectId": 1,
		    "assigneeId": 1,
		    "title": "testEdit",
		    "description": "testEdit",
		    "status": "IN_PROGRESS",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1
		}


#### Get Task By Id

 - **Get** request to **'/projects/{parentProjectId}/tasks/{taskId}'** endpoint with valid token in Authorization header, valid parentProjectId parameter and valid taskId parameter will return a task by its id.

	***To see task, the accessing user must be either the project's creator, or/and part of a team assigned to project.***

 - **Example Response**

	Returns the task found by id:
 

		{
		    "id": 1,
		    "parentProjectId": 1,
		    "assigneeId": null,
		    "title": "test",
		    "description": "test",
		    "status": "PENDING",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1
		}



#### Get All Tasks

 - **Get** request to **'/projects/{parentProjectId}/tasks'** endpoint with valid token in Authorization header and valid parentProjectId parameter will return a list with project's tasks.

	***To see task, the accessing user must be either the project's creator, or/and part of a team assigned to project.***

 - **Example Response**

	Returns a list of tasks:
 

		[
		    {
		        "id": 1,
		        "parentProjectId": 1,
		        "assigneeId": null,
		        "title": "testEdit",
		        "description": "testEdit",
		        "status": "PENDING",
		        "dateCreated": "2020-05-12",
		        "creatorId": 1,
		        "dateLastChanged": "2020-05-12",
		        "lastChangeUserId": 1
		    },
		    {
		        "id": 2,
		        "parentProjectId": 1,
		        "assigneeId": null,
		        "title": "test2",
		        "description": "test2",
		        "status": "PENDING",
		        "dateCreated": "2020-05-12",
		        "creatorId": 1,
		        "dateLastChanged": "2020-05-12",
		        "lastChangeUserId": 1
		    }
		]


#### Delete Task

 - **Delete** request to **'/projects/{parentProjectId}/tasks/{taskId}'** endpoint with valid token in Authorization header, valid parentProjectId parameter and valid taskId parameter will delete a task.

	***The accessing user must be project's creator to be able to delete tasks in project.***

#### Assign User to Task

 - **Put** request to **'/projects/{parentProjectId}/tasks/{taskId}/users/{userId}'** endpoint with valid token in Authorization header, valid parentProjectId parameter, valid taskId parameter and valid userId parameter will assign user to a task.

	***The accessing user must be the project's creator to be able to assign users to tasks in project.***

 - **Example Response**

	Returns the task with the newly assigned user:
 

		{
		    "id": 1,
		    "parentProjectId": 1,
		    "assigneeId": 1,
		    "title": "testEdit",
		    "description": "testEdit",
		    "status": "PENDING",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1
		}


#### Remove Assigned User From Task

 - **Delete** request to **'/projects/{parentProjectId}/tasks/{taskId}/users'** endpoint with valid token in Authorization header, valid parentProjectId parameter and valid taskId parameter will remove assigned user from a task.


	***The accessing user must be the project's creator to be able to remove assigned users from tasks in project.***

 - **Example Response**

	Returns the task with no assigned user:
 

		{
		    "id": 1,
		    "parentProjectId": 1,
		    "assigneeId": null,
		    "title": "testEdit",
		    "description": "testEdit",
		    "status": "PENDING",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1
		}


#### Complete Task

 - **Put** request to **'/projects/{parentProjectId}/tasks/{taskId}/finished'** endpoint with valid token in Authorization header, valid parentProjectId parameter and valid taskId parameter will set a task's status to "COMPLETED".

	***The accessing user must be the project's creator to be able to change the status of tasks in project.***

 - **Example Response**

	Returns the task with newly set status:
 

		{
		    "id": 1,
		    "parentProjectId": 1,
		    "assigneeId": 1,
		    "title": "testEdit",
		    "description": "testEdit",
		    "status": "COMPLETED",
		    "dateCreated": "2020-05-12",
		    "creatorId": 1,
		    "dateLastChanged": "2020-05-12",
		    "lastChangeUserId": 1
		}


----------

### Work Logs

#### Create Work Log

 - **Post** request to **'/tasks/{parentTaskId}/worklogs'** endpoint with valid token in Authorization header, valid parentTaskId parameter and valid body (JSON) will create a new work log.

	***The accessing user must be assigned to task to be able to create work log.***

 - **Example Request**


		{
		   "numberOfHours": 11,
		   "dateOfWork": "2020-05-07"
		}


 - **Example Response**

	Returns the newly created work log:
 

		{
		    "id": 1,
		    "taskId": 1,
		    "creatorId": 1,
		    "numberOfHours": 11,
		    "dateOfWork": "2020-05-07"
		}


#### Edit Work Log

 - **Put** request to **'/tasks/{parentTaskId}/worklogs/{workLogId}'** endpoint with valid token in Authorization header, valid parentTaskId parameter, valid workLogId parameter and valid body (JSON) will edit work log.

	***The accessing user must be work log's creator to be able to edit work log.***

 - **Example Request**
 

		{
		   "numberOfHours": 10,
		   "dateOfWork": "2020-03-06"
		}


 - **Example Response**

	Returns the newly edited work log:
 

		{
		    "id": 1,
		    "taskId": 1,
		    "creatorId": 1,
		    "numberOfHours": 10,
		    "dateOfWork": "2020-03-06"
		}


#### Get Work Log By Id

 - **Get** request to **'/tasks/{parentTaskId}/worklogs/{workLogId}'** endpoint with valid token in Authorization header, valid parentTaskId parameter and valid workLogId parameter will return a work log by its id.

	***To see work log, the accessing user must be either the project's creator, or/and part of a team assigned to project.***

 - **Example Response**

	Returns the work log found by id:
 

		{
		    "id": 1,
		    "taskId": 1,
		    "creatorId": 1,
		    "numberOfHours": 10,
		    "dateOfWork": "2020-03-06"
		}


#### Get All Work Logs

 - **Get** request to **'/tasks/{parentTaskId}/worklogs'** endpoint with valid token in Authorization header and valid parentTaskId parameter will return a list with task's work logs.

	***To see work log, the accessing user must be either the project's creator, or/and part of a team assigned to project.***

 - **Example Response**

	Returns a list of work logs:
 

		[
		    {
		        "id": 2,
		        "taskId": 1,
		        "creatorId": 1,
		        "numberOfHours": 5,
		        "dateOfWork": "2020-01-06"
		    },
		    {
		        "id": 1,
		        "taskId": 1,
		        "creatorId": 1,
		        "numberOfHours": 10,
		        "dateOfWork": "2020-03-06"
		    }
		]


#### Delete Work Log

 - **Delete** request to **'/tasks/{parentTaskId}/worklogs/{workLogId}'** endpoint with valid token in Authorization header, valid parentTaskId parameter and valid workLogId parameter will delete a task.

	***The accessing user must be work log's creator to be able to delete work log.***
